package co.id.miniproject.helper

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import co.id.miniproject.ui.base.Constant
import com.squareup.picasso.Picasso

fun ImageView.loadImageUrl(url: String?) {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()

    Picasso
        .get()
        .load(url)
        .placeholder(circularProgressDrawable)
        .into(this)
}

fun toast(v : Context, message:String){
    Toast.makeText(
        v,
        message,
        Toast.LENGTH_SHORT
    ).show()
}