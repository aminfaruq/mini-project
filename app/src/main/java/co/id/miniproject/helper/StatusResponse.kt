package co.id.miniproject.helper


enum class StatusResponse {
    SUCCESS,
    EMPTY,
    ERROR
}