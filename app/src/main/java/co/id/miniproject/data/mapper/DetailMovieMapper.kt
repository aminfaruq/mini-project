package co.id.miniproject.data.mapper

import co.id.miniproject.data.model.DetailMovieModel
import co.id.miniproject.domain.DetailMovieDomain

class DetailMovieMapper : BaseMapper<DetailMovieModel, DetailMovieDomain> {
    override fun mapToDomain(model: DetailMovieModel): DetailMovieDomain {
        return DetailMovieDomain(
            model.id,
            model.backdrop_path,
            model.original_title,
            model.overview,
            model.poster_path,
            model.release_date,
            model.vote_average
        )
    }

    override fun mapToModel(domain: DetailMovieDomain): DetailMovieModel {
        return DetailMovieModel(
            domain.id,
            domain.backdrop_path,
            domain.original_title,
            domain.overview,
            domain.poster_path,
            domain.release_date,
            domain.vote_average
        )
    }
}