package co.id.miniproject.data.model

import com.google.gson.annotations.SerializedName

data class DetailMovieModel (
    @SerializedName("id")
    val id: Int,
    @SerializedName("backdrop_path")
    val backdrop_path: String,
    @SerializedName("original_title")
    val original_title: String,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("poster_path")
    val poster_path: String,
    @SerializedName("release_date")
    val release_date: String,
    @SerializedName("vote_average")
    val vote_average: Double
)