package co.id.miniproject.data.repository

import co.id.miniproject.data.mapper.MovieMapper
import co.id.miniproject.data.service.ApiInterface
import co.id.miniproject.domain.MovieDomain
import io.reactivex.Single

class MovieRepositoryImpl(
    private val service: ApiInterface,
    private val movieMapper: MovieMapper
) : MovieRepository{
    override fun getMovies(
        apiKey: String,
        language: String,
        shortBy: String,
        page: Int
    ): Single<List<MovieDomain>> {
        return service.getMovies(apiKey,language,shortBy,page).map {
            it.results?.let { it1 -> movieMapper.mapToListDomain(it1) }
        }
    }
}