package co.id.miniproject.ui.detail

import androidx.lifecycle.MutableLiveData
import co.id.miniproject.BuildConfig
import co.id.miniproject.data.repository.DetailMovieRepository
import co.id.miniproject.domain.DetailMovieDomain
import co.id.miniproject.helper.RxUtils
import co.id.miniproject.ui.base.BaseViewModel
import co.id.miniproject.ui.base.Constant

sealed class DetailState
data class DetailDataLoaded(val detailMovieDomain: DetailMovieDomain) : DetailState()
data class ErrorState(var msg: String?) : DetailState()

class DetailMovieVM(private val repository: DetailMovieRepository) : BaseViewModel() {

    val detailMovieState = MutableLiveData<DetailState>()

    fun getDetailMovie(id: String) {
        compositeDisposable.add(
            repository.getDetailMovie(id, BuildConfig.API_KEY, Constant.LANG)
                .compose(RxUtils.applySingleAsync())
                .subscribe({ result ->
                    detailMovieState.value = DetailDataLoaded(result)
                }, this::onError)
        )
    }

    override fun onError(error: Throwable) {
        detailMovieState.value = ErrorState(error.message)
    }
}