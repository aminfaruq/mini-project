package co.id.miniproject.ui.movie

import androidx.lifecycle.MutableLiveData
import co.id.miniproject.BuildConfig
import co.id.miniproject.data.repository.MovieRepository
import co.id.miniproject.domain.MovieDomain
import co.id.miniproject.helper.RxUtils
import co.id.miniproject.ui.base.BaseViewModel
import co.id.miniproject.ui.base.Constant

sealed class MovieState
data class ErrorState(var msg: String?) : MovieState()
data class MovieDataLoadedState(val movieDomain: List<MovieDomain>) : MovieState()
object LoadingState : MovieState()
object LastPageState : MovieState()
object DataNotFoundState : MovieState()
class MovieVM(private val repository: MovieRepository) : BaseViewModel() {

    val movieState = MutableLiveData<MovieState>()

    fun getMovies(page: Int) {
        movieState.value = LoadingState// Loading state
        compositeDisposable.add(
            repository.getMovies(BuildConfig.API_KEY, Constant.LANG, Constant.SORT_BY, page)
                .compose(RxUtils.applySingleAsync())
                .subscribe({ result ->
                    if (result.isNotEmpty()) {
                        movieState.value = MovieDataLoadedState(result) // Data Found State
                    } else {
                        if (page == 1){
                            movieState.value = DataNotFoundState // Data Not Found State
                        }else{
                            movieState.value = LastPageState // Last Page State
                        }
                    }
                }, this::onError)
        )
    }

    override fun onError(error: Throwable) {
        movieState.value = ErrorState(error.message) // Error state
    }

}
