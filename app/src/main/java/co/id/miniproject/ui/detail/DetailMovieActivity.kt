package co.id.miniproject.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import co.id.miniproject.R
import co.id.miniproject.domain.DetailMovieDomain
import co.id.miniproject.helper.loadImageUrl
import co.id.miniproject.ui.base.Constant
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_movie.*
import kotlinx.android.synthetic.main.item_movie.*
import org.koin.android.ext.android.inject

class DetailMovieActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_MOVIE = "EXTRA_MOVIE"
    }

    private val viewModel: DetailMovieVM by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)

        val extras = intent.extras
        val idMovie = extras?.getString(EXTRA_MOVIE)
        viewModel.detailMovieState.observe(this, startObserver)
        idMovie?.let { viewModel.getDetailMovie(it) }

    }

    private val startObserver = Observer<DetailState> { detailMovieState ->
        when (detailMovieState) {

            is DetailDataLoaded -> {
                val datamovie = detailMovieState.detailMovieDomain
                imgDetailMovie.loadImageUrl(Constant.LINK_IMAGE + datamovie.poster_path)
                tvDetailName.text = datamovie.original_title
                tvDetailDesc.text = datamovie.overview
                supportActionBar?.title = datamovie.original_title
            }

            is ErrorState -> {

            }
        }
    }
}
